<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package ibpromo
 */

get_header(); ?>
	
	<div id="primary" class="content-area">

		<main id="main" class="site-main">

			<?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/content', 'page' );

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.
			?>
			
			<div class="cta-grid">
				<section class="cta-grid-item"><?php  get_template_part("/inc/cta-sm-product-a"); ?></section>
				<section class="cta-grid-item cta-grid-item-rspan"><?php  get_template_part("/inc/cta-sm-product-b"); ?></section>
				<section class="cta-grid-item"><?php  get_template_part("/inc/cta-sm-product-c"); ?></section>
			</div>

			<section style="margin-bottom: -60px;">
				<h2 class="h2-custom"><span>New Items</span></h2>
				<?php echo do_shortcode('[featured_products per_page="4" columns="4"]'); ?>
			</section>

		</main><!-- #main -->
	</div><!-- #primary -->


<?php
// get_sidebar();
get_footer();
