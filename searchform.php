<form action="/" method="get" class="navbar-form navbar-right search-form">
	<div class="input-group">
	  <input type="text" class="form-control" placeholder="Search for..." name="s" id="search" value="<?php the_search_query(); ?>">
	  <span class="input-group-btn">
		  <button type="submit" class="btn btn-default" id="basic-addon2"><span class="sr-only">Search</span><i class="fa fa-search" aria-hidden="true"></i> </button>
	  </span>
	</div>
</form>