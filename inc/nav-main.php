<?php $home_title = get_the_title( get_option('page_on_front') ); ?>
<nav class="navbar navbar-inverse">
  <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#main-navbar">
          <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars" aria-hidden="true"></i>
        </button>
      </div>

      <div id="main-navbar" class="collapse navbar-collapse">
        
        <?php wp_nav_menu( array(
          'theme_location'    => 'primary-menu',
          'depth'             => 2,
          'menu_class'        => 'nav navbar-nav',
          'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
          'walker'            => new wp_bootstrap_navwalker())
        ); ?>

        <?php wp_nav_menu( array(
          'theme_location'    => 'utility-menu',
          'depth'             => 1,
          'menu_class'        => 'nav navbar-nav visible-xs-block',
          'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
          'walker'            => new wp_bootstrap_navwalker())
        ); ?>
       
        
        <?php get_search_form(); ?>
        
      </div>

  </div>
</nav>
