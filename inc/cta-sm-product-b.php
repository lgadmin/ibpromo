<?php $image = wp_get_attachment_image_src(get_field('b-cta-image'), 'full'); ?>

<a class="well well-b" href="<?php echo the_field('b-link'); ?>">

	<div class="img-cont">
		<img src="<?php echo $image[0]; ?>" alt="<?php echo get_the_title(get_field('image_test')) ?>" />
	</div>

	<div class="well-body">
		<button><?php echo the_field('b-button_label'); ?></button>
	</div>

</a>  