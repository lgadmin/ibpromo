<?php $image = wp_get_attachment_image_src(get_field('a-cta-image'), 'full'); ?>

<a class="well well-a" href="<?php echo the_field('a-link'); ?>">

	<div class="img-cont">
		<img src="<?php echo $image[0]; ?>" alt="<?php echo get_the_title(get_field('image_test')) ?>" />
	</div>

	<div class="well-body">
		<button><?php echo the_field('a-button_label'); ?></button>
	</div>

</a>   