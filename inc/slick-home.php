<?php  
	$myimages = array(
		'image_1' => get_field('image_one'), 
		'image_2' => get_field('image_two'), 
		'image_3' => get_field('image_three'), 
	);
?>

<div class="slidewrap">
	
	<section class="main-slider"> 
		<?php foreach ($myimages as $i): ?>
			<?php if($i) : ?>
				<div><img data-lazy="<?php echo $i['url']; ?>" alt="<?php echo $i['alt']; ?>" /></div>
			<?php endif ?>
		<?php endforeach ?>
	</section>
	
	<div class="slider-content">
		<div>
			<h2><span>Our goal</span> is to help <br><small>your brand speak for you.</small></h2>
		</div>
	</div>

</div>


