<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package ibpromo
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'ibpromo' ); ?></a>

	<header id="masthead" class="site-header">
		<div class="site-branding">

			<div id="sitelogo"><?php the_custom_logo(); ?></div>
			
			<div class="sitecontact">
				<ul class="list-inline myinline-list">
					<li><a href="#">604.537.3493</a></li>
					<li class="text-danger">/</li>
					<li><a href="#">info@ibpromo.com</a></li>
				</ul>
			</div>

			<div class="sitetitle">
				<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
				<?php
					$description = get_bloginfo( 'description', 'display' );
					if ( $description || is_customize_preview() ) : ?>
						<p class="site-description"><?php echo $description; /* WPCS: xss ok. */ ?></p>
				<?php endif; ?>
			</div>

			<div class="nav-utility">
				<?php  get_template_part("/inc/nav-utility"); ?>
			</div>

		</div><!-- .site-branding -->

	</header><!-- #masthead -->

	<?php  get_template_part("/inc/nav-main"); ?>
	
	<?php 
		if( is_front_page() ) {
			get_template_part('inc/slick-home');
		} 
	?>

	<div id="content" class="site-content">
