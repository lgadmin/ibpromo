<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package csawest
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
	
		<div class="bg-dark">
			<div class="site-footer-content">
				<div class="nav-footer">
					<h2>Quick Links</h2>
					<?php  get_template_part("/inc/nav-footer"); ?>
				</div>

				<div class="footer-logo">
					<?php the_custom_logo(); ?>
				</div>

				<div class="footer-contact" class="text-right">
					<h2 class="text-right">Contact Us</h2>
					
					
<!-- 						<address class="text-right">
							<code>
							1355 Market Street, Suite 900<br>
							San Francisco, CA 94103<br>
							</code>
						</address> -->

						<address class="text-right">
							<span>Phone</span>&nbsp;(604) 537-3493<br>
						</address>
					
				</div>
			</div>
		</div>

		<div class="bg-darker">
			<div class="site-info">
				<div class="lg-graphics">
				<p class="small">&copy; <?php echo date("Y"); ?> <?php bloginfo( 'name' ); ?> All Rights Reserved.</p>
				</div>
				
				<div class="site-legal"><?php get_template_part("/inc/site-footer-longevity"); ?></div>
			</div>
		</div>
	
	</footer><!-- #colophon -->

</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
