// Slick Slider
jQuery(document).ready(function(){
	jQuery('.main-slider').slick({
		lazyLoad: 'ondemand',
		dots: true,
		fade: true,
		cssEase: 'linear',
		infinite: false,
		autoplayspeed: 3000,
		slidesToShow: 1,
		autoplay: true,
		slidesToScroll: 1,
	});
});
